#ifndef NODE_H
#define NODE_H

typedef struct node {
   void        *data;
   struct node *next;
   struct node *prev;
} node;

typedef struct list {
   node  *head;
   node  *tail;
} list;

void   init_list     (list *d);           // initialize the list
void   clear_list    (list *d);           // clear list
int    append_list   (list *d, list *s);  // push list of nodes to another list of nodes (d = destination, s = source)
void   print_list    (list *d, int lvl);  // display list
int    push_node     (list *d, node *s);  // push a node to the list
node  *pop_node      (list *s);           // pop the latest node from the list
int    del_node      (node *n);           // delete a node [note: this places the node in the trash list, ps: you must pop the node first]
node  *new_node      ();                  // create a new node if trash is empty, otherwise pop a node from trash

#endif