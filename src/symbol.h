#ifndef SYMBOL_H
#define SYMBOL_H

   typedef struct symbol {
      char        *name;
      int          line;
      token        tok;
      
      typedef union value {
         int   i; // is this a number?
         char *s; // is this a string?
         list *m; // is this a macro?
      } value;
      
   } symbol;

#endif