#include "asm.h"

#define can_execute(p) p->state != state_macro_def && p->skip == 0
#define peek(p) p->curr

int org = 0;
parser *inc = NULL;

parser *find_parser(char *name) {
   parser *p = inc;
   while (p) {
      if (strcmp(name, p->name) == 0) {
         return p;
      }
      p = p->next;
   }
   return NULL;
}

void error(parser *p, char *s) {
   printf("* line %d, error: %s\n", p->line, s);
   p->state = state_error;
}



void call_macro(parser *p, symbol *call) {
   
}

void atom(parser *p) {
   symbol *x, *y;
   token type = tk_invalid;
   
   printf("atom time\n");
      
   x = peek(p);
   
   // LPAREN expr RPAREN, nested statement
   if (accept(p, tk_lparen)) {
      
      expr(p);
      expect(p, tk_rparen);
   
   // DOT NUMBER, local macro variable
   } else if (accept(p, tk_dot)) {
      
      if (accept(p, tk_num)) {
         
         // todo: grab local variable
         
      } else {
         error(p, "invalid local variable.");
      }

   // ID (LPAREN expr (COMMA expr)* RPAREN)?
   } else if (accept(p, tk_id)) {
      
      // ID LPAREN expr (COMMA expr)* RPAREN, macro call
      if (accept(p, tk_lparen)) {
         // grab the current position of the stack
         y = peek_stack();
         
         if (accept(p, tk_rparen)) {
            
         } else {

            expr(p);
            
            while (accept(p, tk_comma)) {
               expr(p);
            }
            
            expect(p, tk_rparen);
            
         }
         
         // execute the macro as long as we're not in a definition and we aren't in skip condition
         if (can_execute(p)) {
            
            // todo: handle return value
            call_macro(p, x);
         }
         
         // return the stack to the previous position
         ret_stack(y);
      
      // ID, variable
      } else {
         
         if (x->str) {
            y = new_symbol(tk_str);
            y->str = x->str;
         } else {
            y = new_symbol(tk_num);
            y->val = y->val;
         }
         
         psh_stack(y);
         
      }
   
   // string, number, and built in functions
   } else {
      
      printf("test: ");
      
      switch (p->tok) {
         case tk_num:
         case tk_str:

            
            
            x = new_symbol();
            x->tok = p->tok;
            
            accept(p, p->tok);
            
            if (can_execute(p)) {
               psh_stack(x);
            }
            
            break;
         
         case tk_org:
         case tk_cline:
         
            accept(p, p->tok);
            expect(p, tk_lparen);
            expect(p, tk_rparen);
            
            if (can_execute(p)) {
               y = new_symbol(tk_num);
               
               if (p->tok == tk_org) { y->val = org;  }
               else                  { y->val = p->line; }
               
               psh_stack(y);
            }
            
            break;

         case tk_len: // return length of string
         case tk_asc: // return ascii value of first character of string
         case tk_int: // return integer value of string
         
            type = x->tok;
            accept(p, x->tok);
            expect(p, tk_lparen);
            expr(p);
            
            if (accept(p, tk_comma)) {
               error(p, "this function only accepts one argument.");
               return;
            }
            
            expect(p, tk_rparen);
            
            if (can_execute(p)) {
               
               y = pop_stack();
               
               if (y->str) {
                  y->val = str2val(type, y->str);
                  y->str = NULL;
               } else {
                  error(p, "this function requires a string, not an integer.");
                  return;
               }
               
               psh_stack(y);
               
            }
         
            break;
         
         case tk_chr: // return single character string from value (ascii value)
         case tk_bin: // return binary string from integer value
         case tk_oct: // return octal string from integer value
         case tk_dec: // return decimal string from integer value
         case tk_hex: // return hexadecimal string from integer value
         
            type = x->tok;
            accept(p, x->tok);
            expect(p, tk_lparen);
            expr(p);
            
            if (accept(p, tk_comma)) {
               error(p, "this function only accepts one argument.");
               return;
            }
            
            expect(p, tk_rparen);
            
            if (can_execute(p)) {
               
               y = pop_stack();
               
               if (y->str) {
                  error(p, "this function requires an integer value, not a string.");
                  return;
               } else {
                  y->str = val2str(type, y->val);
               }
            
               psh_stack(y);
               
            }
            
      }
      
   }

}

// expression evaluation, to keep things simple, there is no operator precedence, use parentheses to set order 
void expr(parser *p) {
   symbol *x, *y, *z;
   
   while (p->state != state_error) {
      
      x = peek(p);
      
      // unary operators
      switch (x->tok) {
         case tk_sub:
         case tk_lnot:
         case tk_bnot:
         
            accept(p, x->tok);
            
            if (can_execute(p)) {
               y = pop_stack();
               
               if (y->str) {
                  error(p, "operand is a string, cannot perform this operation on strings.");
                  return;
               }
               
               switch (x->tok) {
                  case tk_sub:   y->val = -y->val; break;
                  case tk_lnot:  y->val = !y->val; break;
                  case tk_bnot:  y->val = ~y->val; break;
               }
               psh_stack(y);
            }
      }
      
      // there must be an operand
      atom(p);
      
      x = peek(p);
      
      // binary operators:
      switch (x->tok) {
         case tk_add:
         case tk_sub:
         case tk_band:
         case tk_bor:
         case tk_bxor:
         case tk_shl:
         case tk_shr:
         case tk_mul:
         case tk_div:
         case tk_mod:
         case tk_ne:
         case tk_eq:
         case tk_lr:
         case tk_le:
         case tk_gr:
         case tk_ge:
         case tk_land:
         case tk_lor:
         
            accept(p, x->tok);
            
            // execute binary operator
            if (can_execute(p)) {
               y = pop_stack();
               z = pop_stack();
               
               if (y->str) {
                  error(p, "left operand is a string, cannot perform this operation on strings.");
                  return;
               } else if (z->str) {
                  error(p, "right operand is a string, cannot perform this operation on strings.");
                  return;
               } else if (y->str && z->str) {
                  error(p, "both operands are strings, cannot perform this operation on strings.");
                  return;
               }
               
               switch (x->tok) {
                  case tk_add:   z->val = z->val +  y->val; break;
                  case tk_sub:   z->val = z->val -  y->val; break;
                  case tk_band:  z->val = z->val &  y->val; break;
                  case tk_bor:   z->val = z->val |  y->val; break;
                  case tk_bxor:  z->val = z->val ^  y->val; break;
                  case tk_shl:   z->val = z->val << y->val; break;
                  case tk_shr:   z->val = z->val >> y->val; break;
                  case tk_mul:   z->val = z->val *  y->val; break;
                  case tk_div:   z->val = z->val /  y->val; break;
                  case tk_mod:   z->val = z->val %  y->val; break;
                  case tk_ne:    z->val = z->val != y->val; break;
                  case tk_eq:    z->val = z->val == y->val; break;
                  case tk_lr:    z->val = z->val <  y->val; break;
                  case tk_le:    z->val = z->val <= y->val; break;
                  case tk_gr:    z->val = z->val >  y->val; break;
                  case tk_ge:    z->val = z->val >= y->val; break;
                  case tk_land:  z->val = z->val && y->val; break;
                  case tk_lor:   z->val = z->val || y->val; break;
               }
               psh_stack(z);
            }
         default: return;
      }
      
      
   }
}

void stmt(parser *p) {
   parser *q;
   symbol *x, *y;
   state  prev_state;
   int step = 0;

   while (p->state != state_error) {
      
      step = 0;
      x = peek(p);
      prev_state = p->state;
      
      // DEF ID LN stmt END LN
      if (accept(p, tk_def)) {
         
         if (p->state != state_normal) {
            error(p,"cannot define a macro in a macro.");
            break;
         }
      
         expect(p, tk_id);
         expect(p, tk_line);
         
         if (can_execute(p)) { p->state = state_macro_def; }
         stmt(p);
         if (p->state == state_error) { return; }
         p->state = state_normal;
         
         expect(p, tk_end);
         expect(p, tk_line);

      // ORG expr LN
      } else if (accept(p, tk_org)) {
         
         expr(p);
         
         x = pop_stack();
         if (x) { if (can_execute(p)) { org = x->val; } }
         else   { error(p, "expression did not return any value."); }
         
         expect(p, tk_line);
         
      // INC str LN
      } else if (accept(p, tk_return)) {
      
         // include should not be accepted in a macro definition
         if (p->state == state_macro_def) {
            error(p, "include cannot be inside a macro");
            return;
         }
         
         expect(p, tk_str);
         x = p->curr;
         expect(p, tk_line);
         
         // only execute if condition was met previously
         if (can_execute(p)) {
            // check to see if this file has already been included (and is not this exact file)
            q = find_parser(x->str);
            
            if (q) {
               error(p, "file has already been included.");
               return;
            }
            
            // attempt to load the file
            q = parse(x->str);
            
            // append symbols from q to p
            if (q) {
               append_symbol(p->obj_macro, q->obj_macro);
               append_symbol(p->sym_table, q->sym_table);
               append_symbol(p->str_table, q->str_table);
               
            } else {
               error(p, "file did not parse.");
               return;
            }
            
         }
      
      // IF expr LN stmt (ELIF expr LN stmt)* (ELSE LN stmt)? END LN
      } else if (accept(p, tk_if)) {
         
         expr(p);

         x = pop_stack();
         if (x) { if (can_execute(p)) { p->skip = x->val == 0; } }
         else   { error(p, "expression did not return any value."); }
         
         expect(p, tk_line);
         stmt(p);
         if (p->state == state_error) { return; }
         
         while (accept(p, tk_elif)) {
            expr(p);
            
            x = pop_stack();
            if (x) { if (can_execute(p)) { p->skip = x->val == 0; } }
            else   { error(p, "expression did not return any value."); }
            
            expect(p, tk_line);
            stmt(p);
            if (p->state == state_error) { return; }
         }
         
         if (accept(p, tk_else)) {
            if (can_execute(p)) { p->skip = 1; }
            
            expect(p, tk_line);
            stmt(p);
            if (p->state == state_error) { return; }
         }

         p->skip = 0;
         
         expect(p, tk_end);
         expect(p, tk_line);
      
      // RETURN expr? LN
      } else if (accept(p, tk_return)) {
      
         // return should not be accepted in normal mode
         if (p->state == state_normal) {
            error(p, "return cannot be used outside of a macro.");
            return;
         }
         
         // this return has an expression
         if (!accept(p, tk_line)) {
            expr(p);
            
            // now execute this return
            if (can_execute(p)) {
               p->state = state_normal;
               return;
            }
            
            expect(p, tk_line);
         }
      
      // LN
      } else if (accept(p, tk_line)) {
      
      // ID (COLON? stmt | expr (COMMA expr)*) LN
      } else if (accept(p, tk_id)) {
         
         // ID COLON stmt, this is how you define a label
         if (accept(p, tk_colon)) {
            
            stmt(p);
         
         // ID ASSIGN expr LN
         } else if (accept(p, tk_set)) {
            
            expr(p);
            expect(p, tk_line);
         
         // ID (expr (COMMA expr)*)? LN
         } else {
            
            if (accept(p, tk_line)) {
               
            } else {

               expr(p);
               
               while (accept(p, tk_comma)) {
                  expr(p);
               }
               
               expect(p, tk_line);
               
            }
            
            // execute the macro as long as we're not in a definition and we aren't in skip condition
            if (can_execute(p)) {
               call_macro(p, x);
            }
            
         }
         
      } else {
         
         switch (x->tok) {
            
            // both ERROR and PRINT have common functionality, error just includes error output + stopping execution
            // ERROR expr (COMMA expr)* LN
            case tk_error:
            
               if (can_execute(p)) {
                  error(p, "error command:");
               }
            
            // PRINT expr (COMMA expr)* LN
            case tk_print:
            
               accept(p, x->tok);
            
               expr(p);
         
               if (can_execute(p)) {
                  print_symbol(pop_stack(), 0);
               }
               
               while (accept(p, tk_comma)) {
                  
                  expr(p);
                  
                  if (can_execute(p)) {
                     print_symbol(pop_stack(), 0);
                  }
               }
               expect(p, tk_line);
               
               if (can_execute(p)) {
                  if (p->state == state_error) { return; }
               }
               
               break;

            // write pointer commands
            // ... expr (COMMA expr)* LN
            case tk_word:   if (step == 0) { step = 4; }
            case tk_half:   if (step == 0) { step = 2; }
            case tk_byte:   if (step == 0) { step = 1; }
            case tk_string:
            
               accept(p, x->tok);
               
               expr(p);
               
               y = pop_stack();
               if (step == 0) { step = strlen(y->str); }
               
               if (can_execute(p)) {
                  org += step;
                  
                  if (p->state == state_output) {
                     // todo: actually write output
                  }
                  
                  if (x->tok == tk_string) { step = 0; }
               }
               
               while (accept(p, tk_comma)) {
                  
                  expr(p);
                  
                  y = pop_stack();
                  if (step == 0) { step = strlen(y->str); }
                  
                  if (can_execute(p)) {
                     org += step;
                  
                     if (p->state == state_output) {
                        // todo: actually write output
                     }
                     
                     if (x->tok == tk_string) { step = 0; }
                  }
               }
               
               expect(p, tk_line);
               break;
         }
         
      }
      
   }
}

// parse a file
parser *parse(char *name) {
   
   parser *p;
   FILE *fp;
   
   // attempt to open file for parsing
   fp = fopen(name, "r");
   if (!fp) { printf("* error: invalid filename '%s'.\n", name); return NULL; }
   
   // initialize parser structure
   p = (parser *) malloc(sizeof(parser));
   p->name = name;
   p->fp = fp;
   p->line = 1;
   p->skip = 0;
   
   // add this parser to parser list
   if (inc) {
      p->next = inc;
   }
   inc = p;
   
   // set up string and character pointers
   p->sp = p->sb;
   p->cp = p->cb - 1;

   p->call      = NULL;
   p->next      = NULL;
   p->curr      = NULL;
   p->head      = NULL;
   p->tail      = NULL;
   p->sym_table = NULL;
   p->str_table = NULL;
   p->obj_macro = NULL;

   p->state = state_normal;   // start in normal state
   rdch(p);                   // read in the first character
   p->tok = rdtk(p);          // read in the first token
   stmt(p);                   // start executing
   
   /*
   printf("\n###################\n%s parsed\n###################\n", p->name);
   printf("\nsymbol table:\n");
   lssym(p->sym_table, 1);
   printf("\nstring table:\n");
   lssym(p->str_table, 1);
   printf("\noutput:\n");
   lssym(p->obj_stack, 1);
   printf("\n###################\nwrite %s output:\n###################\n", p->name);
   */

   if (p->state == state_error) {
      if (p->fp) { fclose(p->fp); }
      free(p->name);
      free(p);
      return NULL;
   }
   
   return p;
}