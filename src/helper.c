#include "asm.h"

char tmp[64];

int str2val(token type, char *n) {
   int v = 0;
   char *c = n + strlen(n) - 1;
   
   switch(type) {
      case tk_len: // len()
         return strlen(n);
      case tk_asc: // asc()
         return n[0];
      case tk_int: // int()
         
         switch (*c) {
      
            // octal
            case 'o':
               while (n < c) { if (*n >= '0' && *n <= '7') { v *=  8; v += *n - '0'; } n++; }
               return v;
            
            // hexadecimal
            case 'h':
               while (n < c) {
                  if (*n >= '0' && *n <= '7') { v *= 16; v += *n - '0'; }
                  else if (*n >= 'a' && *n <= 'f') { v *= 16; v += *n - 'a' + 10; }
                  else if (*n >= 'A' && *n <= 'F') { v *= 16; v += *n - 'A' + 10; }
                  n++;
               }
               return v;
            
            // binary
            case 'b':
               while (n < c) { if (*n >= '0' && *n <= '1') { v *=  2; v += *n - '0'; } n++; }
               return v;
            
            // decimal
            default :
               while (n <= c) { if (*n >= '0' && *n <= '9') { v *= 10; v += (*n) - '0'; } n++; }
               return v;
         }
         
      default:
         return 0;
   }
}

char *val2str(token type, int v) {
   tmp[63] = 0x00;
   int i = 62;
   
   switch (type) {
      case tk_bin:
      case tk_oct:
      case tk_hex:
      case tk_dec:
   
         switch (type) {
            case tk_bin: tmp[i] = 'b'; i--; break;
            case tk_oct: tmp[i] = 'o'; i--; break;
            case tk_hex: tmp[i] = 'h'; i--; break;
         }
      
         while (v > 0) {
            switch (type) {
               case tk_bin:                     tmp[i] = ((v -  0) &  2) + '0';   v >>= 1; break;
               case tk_oct:                     tmp[i] = ((v -  0) &  8) + '0';   v >>= 2; break;
               case tk_hex: if ((v & 15) > 9) { tmp[i] = ((v - 10) & 15) + 'A'; }
                            else              { tmp[i] = ((v -  0) & 15) + '0'; } v >>= 4; break;
               default:                         tmp[i] = (v % 10) + '0'; v /= 10;
            }
            i--;
         }
         i++;
      
      case tk_chr:
         tmp[i] = v & 0xFF;
   }
   
   return &tmp[i];
      
}
