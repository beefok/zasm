#ifndef ASM_H
#define ASM_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "node.h"

typedef enum {
   state_error, state_normal, state_macro_def, state_macro_run, state_output
} state;



extern const char *tk_name[tk_enum_length];

typedef struct _p {
   FILE        *fp, *wp;
   char        *name;
   int          line;
   char        *cp, cb[4096];
   char        *sp, sb[4096];
   int         skip;
   state       state;
   token       tok;
   
   symbol      *curr, *prev, *call;
   
   list        *str_table;
   list        *sym_table;
   list        *out_macro;
} parser;

// parser functions
parser   *find_parser   (char *name);
void      expr          (parser *p);
parser   *parse         (char *name);
// lexer functions
char     *getstr        (parser *p);
token     rdtk          (parser *p);
void      rdch          (parser *p);
void      revsp         (parser *p);
void      rstsp         (parser *p);
void      print_token   (token  tk);
// symbol functions
symbol   *psh_symbol    (char *name);
symbol   *psh_string    (char *str);
symbol   *new_symbol    ( );
void      del_symbol    (symbol *s); 
void      print_symbol  (symbol *s, int lvl);
symbol   *peek_stack    ( );
symbol   *pop_stack     ( );
void      psh_stack     (symbol *s);
// helper functions
int       str2val       (token type, char *n);
char     *val2str       (token type, int v);

#endif