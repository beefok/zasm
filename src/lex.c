#include "lex.h"

const char *tk_name[tk_enum_length] =
{
   "invalid", "end of file", "identifier", "number", "string", "line", "directive", "label", "comma", "left paren", "right paren",
   "assign", "plus", "minus", "multiply", "divide", "modulus", "bitwise not", "bitwise and", "bitwise or", "bitwise xor", "shift left", "shift right",
   "not equal", "equal", "lesser", "lesser or equal", "greater", "greater or equal", "logical not", "logical and", "logical or",
   "end", "define", "if", "elif", "else", "include", "include binary", "string", "word", "half", "byte", "return", "error", "print", ".org",
   "line", "dec", "bin", "oct", "hex", "val", "len", "chr", "asc"
};

//void revsp(parser *p) { popsp(); }
//void rstsp(parser *p) { p->sp = p->sb; *(p->sp++) = *p->cp; p->tok = rdtk(p); }

char *lex_str(lexer *k) {
   char *c = (char *) malloc(strlen(k->sb));
   strcpy(c, k->sb);
   return c;
}

void print_token(token tk) {
   printf("%s\n", tk_name[tk]);
}

void rdch(lexer *k) {
   if (*k->cp == '\n') { k->ln++; }
   int x = ((++k->cp) - k->cb) & (BUFFER_LENGTH-1);
   
   if (x == 0) {
      if (k->fp) {
         x = fread(k->cb, 1, BUFFER_LENGTH, k->fp);
         printf("* read in %d bytes from '%s'", x, k->fn);
         if (x < BUFFER_LENGTH) {
            fclose(k->fp);
            k->fp = NULL;
            printf(" and closed file");
            k->cb[x] = 0x00;
         }
         printf(".\n");
         k->cp = k->cb;
      }
   }
   
   *k->sp = *k->cp;
   x = k->sp - k->sb;
   if (x < (BUFFER_LENGTH-1)) { k->sp++; }
   *k->sp = 0x00;
   
   printf("<%s>", k->sb);
}

token rdtk(lexer *k) {
   char  temp;
   int   len;
   
   switch (*k->cp) {
      case 0x00: rdch(k); return tk_eof;
      case '\n': rdch(k); return tk_line;
      case  ':': rdch(k); return tk_colon;
      case  ',': rdch(k); return tk_comma;
      case  '.': rdch(k); return tk_dot;
      case  '(': rdch(k); return tk_lparen;
      case  ')': rdch(k); return tk_rparen;
      case  '+': rdch(k); return tk_add;
      case  '-': rdch(k); return tk_sub;
      case  '/': rdch(k); return tk_div;
      case  '%': rdch(k); return tk_mod;
      case  '^': rdch(k); return tk_bxor;
      case  '~': rdch(k); return tk_bnot;
      case  '*': rdch(k); return tk_mul;
      case  '=':
         rdch(k);
         if (*k->cp == '=') { rdch(k); return tk_eq; }
         return tk_set;
      case  '!':
         rdch(k);
         if (*k->cp == '=') { rdch(k); return tk_ne; }
         return tk_lnot;
      case  '&':
         rdch(k);
         if (*k->cp == '&') { rdch(k); return tk_land; }
         return tk_band;
      case  '|':
         rdch(k);
         if (*k->cp == '|') { rdch(k); return tk_lor; }
         return tk_bor;
      case  '<':
         rdch(k);
         if (*k->cp == '<') { rdch(k); return tk_shl; }
         if (*k->cp == '=') { rdch(k); return tk_le;  }
         return tk_lr;
      case  '>':
         rdch(k);
         if (*k->cp == '>') { rdch(k); return tk_shr; }
         if (*k->cp == '=') { rdch(k); return tk_ge;  }
         return tk_gr;
      case  '"':
         popsp();
         rdch(k);
         while (*k->cp != 0x00 && *k->cp != '"') { rdch(k); }
         popsp();
         rdch(k);
         return tk_str;
      case '#':
         rdch(k);
         if (*k->cp == '*') {
            rdch(k);
            while (*k->cp != 0x00) {
               if (*k->cp == '*') {
                  rdch(k);
                  if (*k->cp == '#') {
                     rdch(k);
                     break;
                  }
               }
               rdch(k);
            }
         } else {
            while (*k->cp != 0x00 && *k->cp != '\n') { rdch(k); }
         }
         temp = *k->cp;
         k->sp = k->sb;
         *k->sp = temp;
         k->sp++;
         *k->sp = 0x00;
         return rdtk(k);
         
      default  :
      
         if (isWS) {
            while (isWS) { rdch(k); }
            temp = *k->cp;
            k->sp = k->sb;
            *k->sp = temp;
            k->sp++;
            *k->sp = 0x00;
            return rdtk(k);
         }
         
         if (isNM) {
            while (isTX || isNM) { rdch(k); }
            return tk_num;
         }
         
         if (isTX) {
            while (isTX || isNM) { rdch(k); }
            
            temp = *k->cp;
            popsp();
            
            if (strcmp(k->sb,     "end") == 0) { pshsp(); return tk_end;    }
            if (strcmp(k->sb,     "def") == 0) { pshsp(); return tk_def;    }
            if (strcmp(k->sb,      "if") == 0) { pshsp(); return tk_if;     }
            if (strcmp(k->sb,    "elif") == 0) { pshsp(); return tk_elif;   }
            if (strcmp(k->sb,    "else") == 0) { pshsp(); return tk_else;   }
            if (strcmp(k->sb ,    "inc") == 0) { pshsp(); return tk_inc;    }
            if (strcmp(k->sb , "incbin") == 0) { pshsp(); return tk_incbin; }
            if (strcmp(k->sb ,      "s") == 0) { pshsp(); return tk_string; }
            if (strcmp(k->sb ,      "w") == 0) { pshsp(); return tk_word;   }
            if (strcmp(k->sb ,      "h") == 0) { pshsp(); return tk_half;   }
            if (strcmp(k->sb ,      "b") == 0) { pshsp(); return tk_byte;   }
            if (strcmp(k->sb , "return") == 0) { pshsp(); return tk_return; }
            if (strcmp(k->sb ,  "error") == 0) { pshsp(); return tk_error;  }
            if (strcmp(k->sb ,  "print") == 0) { pshsp(); return tk_print;  }
            if (strcmp(k->sb ,    "org") == 0) { pshsp(); return tk_org;    }
            if (strcmp(k->sb ,   "line") == 0) { pshsp(); return tk_cline;  }
            if (strcmp(k->sb ,    "dec") == 0) { pshsp(); return tk_dec;    }
            if (strcmp(k->sb ,    "bin") == 0) { pshsp(); return tk_bin;    }
            if (strcmp(k->sb ,    "oct") == 0) { pshsp(); return tk_oct;    }
            if (strcmp(k->sb ,    "hex") == 0) { pshsp(); return tk_hex;    }
            if (strcmp(k->sb ,    "int") == 0) { pshsp(); return tk_int;    }
            if (strcmp(k->sb ,    "len") == 0) { pshsp(); return tk_len;    }
            if (strcmp(k->sb ,    "chr") == 0) { pshsp(); return tk_chr;    }
            if (strcmp(k->sb ,    "asc") == 0) { pshsp(); return tk_asc;    }
            pshsp(); 
            return tk_id;
         }
   }
   
   return tk_invalid;
}

void lex_step(lexer *k) {
   // push temp character onto string buffer
   k->tk = rdtk(k);
   // pop temp character off string buffer
   
   //void revsp(parser *p) { popsp(); }
}

lexer *lex(char *fn) {
   lexer *k;
   FILE  *fp;
   
   // attempt to open file for lexical analysis
   fp = fopen(fn, "r");
   if (!fp) { printf("* error: invalid filename '%s'.\n", fn); return NULL; }
   
   // initialize lexer structure
   k = (lexer *) malloc(sizeof(lexer));
   k->fn = fn;
   k->fp = fp;
   k->ln = 1;
   
   // set up string and character pointers
   k->sp = k->sb;
   k->cp = k->cb - 1;

   rdch(k);      // read in the first character
   lex_step(k);  // read in the first token
   
   return k;
}

int main() {
   return 0;
}