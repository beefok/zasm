#include "asm.h"

symbol *stack = NULL;
symbol *trash = NULL;

symbol *sym_table = NULL;
symbol *str_table = NULL;
symbol *sym_stack = NULL;

void next_sym(parser *p) {
   token tok;
   
   tok = rdtk(p);
   
   // step the string buffer back by one character
   revsp(p);
   
   switch (tok) {
      case tk_str: p->curr = psh_string(p->sb); break;
      case tk_id : p->curr = psh_symbol(p->sb); break;
   }
   
   printf("accepted token: ");
   print_token(tok);
   printf("\n");
   
   // reset the string buffer and place the top of the character buffer on it
   rstsp(p);
}

int accept(parser *p, token tok) {
   char temp;
   symbol *x, *y;

   switch (p->state) {
      case state_error:
         return NULL;
         
      // between macro defines and normal run, we are grabbing new symbols
      case state_macro_def:
      case state_normal:
         if (sym_stack->tok == tok) {
            next_sym(p);
            return 1;
         }
         return NULL;
      
      // in macro run, we are running symbols from a macro list
      case state_macro_run:
      case state_output:
         return NULL;
   }
}

void expect(parser *p, token tok) {
   if (!accept(p, tok)) { error(p, "unexpected token."); printf("") }
}

void set_symbol(symbol *d, symbol *s) {
   d->name = s->name;
   d->str  = s->str;
   d->val  = s->val;
   d->head = s->head;
   d->tail = s->tail;
}

symbol *psh_string(char *str) {
   // search for string
   symbol *s = str_table;
   while (s) {
      if (strcmp(s->str, str) == 0) { return; }
      s = s->next;
   }
   
   // we didn't a string, allocate a new symbol for the string
   s = new_symbol();
   s->tok = tk_str;
   
   // allocate the string itself
   s->str = (char *) malloc(strlen(str));
   strcpy(s->str, str);
   
   // push symbol to string table
   s->next = str_table;
   str_table = s;
   return s;
}

symbol *psh_symbol(char *name) {
   // search for symbol
   symbol *s = sym_table;
   while (s) {
      if (strcmp(s->name, name) == 0) { return; }
      s = s->next;
   }
   
   // we didn't find a symbol, allocate a new symbol
   s = new_symbol();
   s->tok = tk_id;
   
   // allocate the name for the symbol
   s->name = (char *) malloc(strlen(name));
   strcpy(s->name, name);
   
   // push symbol to symbol table
   s->next = sym_table;
   sym_table = s;
   return s;
}

symbol *new_symbol() {
   symbol *s;
   
   // detect if there is a free symbol in trash
   if (trash) {
      s = trash;
      trash = trash->next;
   } else {
      s = (symbol *) malloc(sizeof(symbol));
   }
   
   // clear trash and/or initialize symbol
   s->line =  0;
   s->val  =  0;
   s->tok  = -1;
   s->name = NULL;
   s->str  = NULL;
   s->next = NULL;
   s->head = NULL;
   s->tail = NULL;
   
   // now return it
   return s;
}

void append_symbol(symbol *d, symbol *s) {
   s->next = d;
   d = s;
}

void del_symbol(symbol *s) {
   s->next = trash;
   trash = s;
}

symbol *peek_stack() {
   return stack;
}

symbol *pop_stack() {
   symbol *x = stack;
   stack = stack->next;
   return x;
}

//#define psh_stack (symbol *s) s->next = stack; stack = s;
void psh_stack(symbol *s) {
   s->next = stack;
   stack = s;
}

void ret_stack(symbol *s) {
   stack = s;
}

void print_symbol(symbol *s, int lvl) {
   for (int i = 0; i < lvl; i++) { printf("   "); }
   
   switch (s->tok) {
      /*case tk_incbin:
         printf("incbin output: %08X, addr %08X\n", s, s->val);
         break;
      case tk_w:
         if (s->head) { printf("word output: %08X, addr %08X\n", s, s->val); print_symbol(s->head, lvl+1); }
         else         { printf("%s\n", s->name); }
         break;
      case tk_h:
         if (s->head) { printf("half output: addr %08X\n", s->val); print_symbol(s->head, lvl+1); }
         else         { printf("%s\n", s->name); }
         break;
      case tk_b:
         if (s->head) { printf("byte output: addr %08X\n", s->val); print_symbol(s->head, lvl+1); }
         else         { printf("%s\n", s->name); }
         break;
      case tk_string:
         if (s->head) { printf("string output: addr %08X\n", s->val); print_symbol(s->head, lvl+1); }
         else         { printf("%s\n", s->name); }
         break;*/
      case tk_id:
         if (s->head)   { printf("macro: %s, addr %08X\n", s->name, s->val); print_symbol(s->head, lvl+1); }
         else           {
            if (s->str) { printf("identifier: %s = \"%s\"\n", s->name, s->str); }
            else        { printf("identifier: %s = %d\n", s->name, s->val);    }
         }
         break;
      case tk_str: printf("string: \"%s\"\n", s->str); break;
      case tk_num: printf("number: %d\n", s->val); break;
   // case tk_comma: printf("output: %d\n", s->val); break;
      default:     printf("%s, addr:%08X, next:%08X\n", tk_name[s->tok], s, s->next);
   }
}