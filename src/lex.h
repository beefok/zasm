#ifndef LEX_H
#define LEX_H

#include <stdio.h>

#define isWS   (*k->cp >=  1  && *k->cp <  33 )
#define isNM   (*k->cp >= '0' && *k->cp <= '9')
#define isTX   (*k->cp >= 'a' && *k->cp <= 'z') || (*k->cp >= 'A' && *k->cp <= 'Z') || (*k->cp == '_')
#define popsp() k->sp--; *k->sp = 0x00;
#define pshsp() *(k->sp++) = temp; *k->sp = 0x00;

#define BUFFER_LENGTH 4096

typedef enum {
   tk_invalid, tk_eof, tk_id, tk_num, tk_str,
   tk_line, tk_dot, tk_colon, tk_comma, tk_lparen, tk_rparen,
   tk_set, tk_add, tk_sub, tk_mul, tk_div, tk_mod, 
   tk_bnot, tk_band, tk_bor, tk_bxor, tk_shl, tk_shr,
   tk_ne, tk_eq, tk_lr, tk_le, tk_gr, tk_ge, tk_lnot, tk_land, tk_lor,
   tk_end, tk_def, tk_if, tk_elif, tk_else, tk_inc, tk_incbin, tk_string, tk_word, tk_half, tk_byte, tk_return, tk_error, tk_print, tk_org,
   tk_cline, tk_dec, tk_bin, tk_oct, tk_hex, tk_int, tk_len, tk_chr, tk_asc,
   tk_enum_length
} token;

typedef struct _l {
   FILE *fp;
   int   ln;
   char *fn;
   char *cp;
   char *sp;
   char  cb[BUFFER_LENGTH];
   char  sb[BUFFER_LENGTH];
   token tk;
} lexer;

void      print_token(token tk);
char     *lex_str    (lexer *k);
lexer    *lex        (char *fn);
void      lex_step   (lexer *k);

#endif