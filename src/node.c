//#include <stdio.h>
#include "node.h"

// node trash can
list trash;

// push list of nodes to another list of nodes (d = destination, s = source)
int append_list(list *d, list *s) {
   //printf("d:%08X, head:%08X, tail:%08X\ns:%08X, head:%08X, tail:%08X\n", d, d->head, d->tail, s, s->head, s->tail);
   if (d) {
      if (s) {
         if (d->tail) { d->tail->next = s->head;}
         d->tail = s->tail;
         //printf("append %08X to %08X\n", s, d);
         return 1;
      }
   }
   return 0;
}

// push a node to the list
int push_node(list *d, node *s) {
   if (d) {
      //printf("push %08X to %08X\n", s, d);
      if (d->tail) {
         d->tail->next = s;
         s->prev = d->tail;
      } else {
         s->prev = NULL;
         d->head = s;
         //printf("head of %08X becomes %08X\n", d, d->head);
      }
      s->next = NULL;
      d->tail = s;
      //printf("tail of %08X becomes %08X\n", d, d->tail);
      return 1;
   }
   //printf("cannot push, invalid list\n");
   return 0;
}

// pop the last node from the list
node *pop_list(list *s) {
   node *n = NULL;
   
   if (s) {      
      if (s->head) {
         n = s->tail;
         s->tail = s->tail->prev;
         
         // if there is only one node in the list, clear the head as well
         if (!s->tail) { s->head = NULL; }
         
         n->prev = NULL;
         n->next = NULL;
         //printf("pop %08X from %08X\n", n, s);
         
         return n;
      } else {
         return NULL;
      }
   }
   return NULL;
}

// delete a node [note: this places the node in the trash list, ps: if this node is part of a list it is removed first]
int del_node(node *n) {
   if (n) {
      // this node is still part of a list, assign the previous node's next reference to this node's next reference
      if (n->prev) { n->prev->next = n->next; }
      //printf("delete %08X, ", n);
      push_node(&trash, n);
      return 1;
   }
   return 0;
}

// create a new node if trash is empty, otherwise pop a node from trash
node *new_node() {
   node *n = pop_node(&trash);
   if (!n) { n = (node *) malloc(sizeof(node)); /*printf("create %08X\n", n);*/ }
   n->data = NULL;
   n->next = NULL;
   n->prev = NULL;
   return n;
}

// initialize the list
void init_list(list *d) {
   d->head = NULL;
   d->tail = NULL;
}

// display list
void print_list(list *d, int lvl) {
   node *n = d->head;
   
   printf("list %08X:\n", d);
   while (n) {
      for (int i = 0; i < lvl; i++) { printf("   "); }
      printf("node %08X, next: %08X\n", n, n->next);
      n = n->next;
   }
   printf("\n");
}

// clear list
void clear_list(list *d) {
   node *n;
   
   //printf("clear list: %08X\n", d);
   
   while (1) {
      n = pop_node(d);
      if (n) {
         del_node(n);
      } else {
         break;
      }
   }
   
   init_list(d);
}
/*
int main() {
   
   list j, k;
   node *x, *a, *b, *c;
   
   init_list(&j);
   init_list(&k);
   init_list(&trash);
   
   printf("trash list: %08X.\n", &trash);
   
   print_list(&k, 1);
   
   a = new_node();
   b = new_node();
   c = new_node();
   
   push_node(&k, a);
   push_node(&k, b);
   push_node(&k, c);
   
   printf("created a list of 3 nodes\n");
   print_list(&k, 1);
   
   x = pop_node(&k);
   del_node(x);
   x = pop_node(&k);
   del_node(x);
   x = pop_node(&k);
   del_node(x);
   
   printf("pop the nodes from the list\n");
   print_list(&k, 1);
   
   printf("they should now be in the trash list\n");
   print_list(&trash, 1);
   
   push_node(&j, new_node());
   push_node(&j, new_node());
   push_node(&j, new_node());
   push_node(&j, new_node());
   push_node(&j, new_node());
   
   printf("push a bunch a new nodes to %08X.\n", &j);
   print_list(&j, 1);
   
   printf("push some new nodes to %08X.\n", &k);
   push_node(&k, new_node());
   push_node(&k, new_node());
   
   print_list(&k, 1);

   append_list(&k, &j);
   
   printf("now append %08X to %08X.\n", &j, &k);
   print_list(&j, 1);
   print_list(&k, 1);
   
   del_node(a);
   del_node(b);
   del_node(c);
   
   printf("for some reason I want to delete %08X, %08X, and %08X.\n", a, b, c);
   print_list(&j, 1);
   print_list(&k, 1);
  
   return 0;
}*/