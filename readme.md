# README #

ZASM, Minimalist Macro Assembler

### What is this repository for? ###

This project is an open-source macro assembler built to assist in the development of new FPGA/ASIC based CPU designs.
Designing an assembler for a new cpu only requires that you create macros for each instruction.
The only commands you have access to are pretty much just file i/o commands to output the object file.

### How do I get set up? ###

* clone the repository, compile the code in your favorite compiler (should be safe on any ansi-c compiler), begin assembling!
* Dependencies: ANSI C

### Who do I talk to? ###

* the designer is beefok, electrodev@gmail.com