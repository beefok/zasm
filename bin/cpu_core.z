#*
   this is the assembler directive/definition of the zcore instruction set
*#
## register defines
r0 = "r0"
r1 = "r1"
r2 = "r2"
r3 = "r3"
r4 = "r4"
r5 = "r5"
r6 = "r6"
r7 = "r7"

## test if this is a register
def isreg
   if   .1 == r0
      return 0
#*
   elif .1 == r1
      return 1
   elif .1 == r2
      return 2
   elif .1 == r3
      return 3
   elif .1 == r4
      return 4
   elif .1 == r5
      return 5
   elif .1 == r6
      return 6
   elif .1 == r7
      return 7*#
   end
   
   error "parameter is not a known register (r0 - r7)"
end

#*
## test if this is in unsigned range (+1 to +8)
def u3
   if .1 > 8
      error "parameter is greater than unsigned immediate range (> +8)"
   elif .1 < 1
      error "parameter is less than unsigned immediate range (< +1)"
   end
   return .1 - 1
end

## test if this is in signed range (-3 to +3)
def s3
   if .1 > 3
      error "parameter is greater than signed immediate range (> +3)"
   elif .1 < -3
      error "parameter is lesser than signed immediate range (< -3)"
   end
   return .1
end

## test if this is in 12-bit unsigned range
def u12
   if .1 > 4096
      error "parameter is greater than 12-bit unsigned immediate range (> 4096)"
   elif .1 < 0
      error "parameter is lesser than 12-bit unsigned immediate range (< 0)"
   end
end

## test if this is in 12-bit unsigned range
def s12
   if .1 > 2047
      error "parameter is greater than 12-bit signed immediate range (> +2047)"
   elif .1 < -2048
      error "parameter is lesser than 12-bit signed immediate range (< -2048)"
   end
end

## test if this is in 6-bit i/o range
def io
   if .1 > 63
      error "parameter is greater than i/o port range (> 63)"
   elif .1 < 0
      error "parameter is lesser than i/o port range (< 0)"
   end
end

## test if this is in 10-bit jump range
def sjmp
   addr = .1 - .org

   if addr > 511 || addr < 512
      error "outside of jump range (> +511)"
   end
   
   return addr
end


######################################################################################################
## instruction word types
######################################################################################################

def xword
   if .3 < 0
      s12(.3)
   else
      u12(.3)
   end
   
   w (.1 << 9) | (isreg(.2) << 6) | ( 4 << 3) | (.4)
   w .3
end

#*

def uword
   w (.1 << 9) | (isreg(.2) << 6) | (u3(.3) << 3) | (.4)
end

def sword
   w (.1 << 9) | (isreg(.2) << 6) | (s3(.3) << 3) | (.4)
end

def rword
   w ((.1 + 1) << 9) | (isreg(.2) << 6) | (ireg(.3) << 3) | (.4)
end

def bword
   w (.1 << 9) | (isreg(.2) << 6) | (io(.3))
end

######################################################################################################

## branch immediate/register

def jx
   xword(0, .1, .2, 0)
end

def ji
   uword(0, .1, .2, 0)
end

def jr
   rword(0, .1, .2, 0)
end

## compare instructions

def eqx
   xword(0, .1, .2, 1)
end

def eqi
   sword(0, .1, .2, 1)
end

def eq
   rword(0, .1, .2, 1)
end

def lrx
   xword(0, .1, .2, 2)
end

def lri
   sword(0, .1, .2, 2)
end

def lr
   rword(0, .1, .2, 2)
end

def grx
   xword(0, .1, .2, 3)
end

def gri
   sword(0, .1, .2, 3)
end

def gr
   rword(0, .1, .2, 3)
end

## memory load/store instructions

def lx
   xword(0, .1, .2, 4)
end

def ld
   rword(0, .1, .2, 4)
end

def sx
   xword(0, .1, .2, 5)
end

def st
   rword(0, .1, .2, 5)
end

## add/sub instructions

def addx
   xword(0, .1, .2, 6)
end

def addi
   uword(0, .1, .2, 6)
end

def add
   rword(0, .1, .2, 6)
end

def subx
   xword(0, .1, .2, 7)
end

def subi
   uword(0, .1, .2, 7)
end

def sub
   rword(0, .1, .2, 7)
end

## shift instructions

def roli
   uword(2, .1, .2, 0)
end

def rol
   rword(2, .1, .2, 0)
end

def rori
   uword(2, .1, .2, 1)
end

def ror
   rword(2, .1, .2, 1)
end

def shli
   uword(2, .1, .2, 2)
end

def shl
   rword(2, .1, .2, 2)
end

def shri
   uword(2, .1, .2, 3)
end

def shr
   rword(2, .1, .2, 3)
end


## bitwise instructions

def movx
   xword(2, .1, .2, 4)
end

def movi
   uword(2, .1, .2, 4)
end

def mov
   rword(2, .1, .2, 4)
end

def andx
   xword(2, .1, .2, 5)
end

def andi
   uword(2, .1, .2, 5)
end

def and
   rword(2, .1, .2, 5)
end

def orx
   xword(2, .1, .2, 6)
end

def ori
   uword(2, .1, .2, 6)
end

def or
   rword(2, .1, .2, 6)
end

def xorx
   xword(2, .1, .2, 7)
end

def xori
   uword(2, .1, .2, 7)
end

def xor
   rword(2, .1, .2, 7)
end

## i/o bus instructions

def get
   .bword(4, .1, .2)
end

def set
   .bword(5, .1, .2)
end

## unconditional jump instruction

def j
   w (6 << 9) | (sjmp(.1))
end
*#
